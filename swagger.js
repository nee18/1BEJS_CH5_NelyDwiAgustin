const swaggerAutogen = require('swagger-autogen')();

const doc = {
  info: {
    title: 'SUPPLY BLOOD',
    description: 'Supply Blood provides blood stock information on PMI and Hospital',
  },
  host: 'localhost:4000',
  schemes: ['http'],
};

const outputFile = './api-documentation/swagger-with-autogen.json';
const endpointsFiles = ['./server.js'];

/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);