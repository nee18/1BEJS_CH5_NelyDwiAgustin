const { body } = require('express-validator');
const { Role } = require('../../models');

module.exports = {
    create: () => [
        body('name')
            .isLength({min:3, max: undefined})
            .withMessage('must be at least 3 chars long'),
        body('city')
            .isLength({min:1, max: undefined})
            .withMessage('must be at least 1 chars long'),
        body('role_id')    
            .isInt()
            .custom((value) => {
            return Role.findOne({
                where : {
                    id : value
                }
            }).then((user) => {
                if(!user) throw new Error(`Role with id ${value} is not found`)
            })
        })
    ]
}