const { body } = require('express-validator');

module.exports = {
    create: () => [
        body('name')
            .toLowerCase()
            .default('new member')
    ]
}