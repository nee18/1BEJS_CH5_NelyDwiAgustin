const { body } = require('express-validator');

module.exports = {
    create: () => [
        body('name')
            .isLength({min:1, max: undefined})
            .withMessage('must be at least 1 chars long'),
        
    ]
}