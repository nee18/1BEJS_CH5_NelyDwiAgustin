const { body } = require('express-validator');
const { Staff } = require('../../models');

module.exports = {
    create: () => [
        body('balance').toInt(),
        body('staff_id').toInt()
    ]
}