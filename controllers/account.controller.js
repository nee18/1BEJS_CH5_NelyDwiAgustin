const { Account } = require('../models');

const getAllAccounts = async (req, res) => {
    try {
        // let { page, row } = req.query;

        // page -= 1;

        const options = {
            attributes: ['id', 'balance', 'staff_id'],
            // offset: page,
            // limit: row,
        };

        const allAccounts = await Account.findAll(options);

        res.status(200).json({
            status: 'success',
            result: allAccounts,
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        }) 
    }
}

const getAccountById = async (req, res) => {
    const foundAccount = await Account.findByPk(req.params.id);

    if (!foundAccount) {
        return res.status(404).json(`Account dengan id ${req.params.id} tidak ditemukan`)
    }
    res.status(200).json({
        status: 'Success',
        result: foundAccount
    })
}

const createAccount = async (req, res) => {
    try {
        const { balance, staff_id } = req.body;
    
        const createdAccount = await Account.create({
            balance: balance,
            staff_id: staff_id
        });
        res.status(201).json({
            status: 'Success',
            result: createdAccount
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
    }
}

const updateAccount = async (req, res) => {
  try{
      const { balance, staff_id } = req.body;
    
      const updatedAccount = await Account.update({
          balance: balance,
          staff_id: staff_id
      }, {
        where: {
          id: req.params.id
        } 
      });
      if (!updatedAccount[0]) {
        res.status(404).json(`Account dengan id ${req.params.id} tidak ditemukan`);
      }
      res.status(200).json({ 
          status: 'success',
          result: updatedAccount
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

const deleteAccount = async (req, res) => {
  try{
      const deletedAccount = await Account.destroy({
          where: {
              id: req.params.id
          }
      });
      if (!deletedAccount) {
        res.status(404).json(`Account dengan id ${req.params.id} tidak ditemukan`);
      }
      res.status(200).json({ 
          status: 'success',
          message: 'Yosha'
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

module.exports = {
  getAllAccounts,
  getAccountById,
  createAccount,
  updateAccount,
  deleteAccount
}