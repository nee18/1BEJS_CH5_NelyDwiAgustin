const { Staff } = require('../models');

const getAllStaffs = async (req, res) => {
    try {
        // let { page, row } = req.query;

        // page -= 1;

        const options = {
            attributes: ['id', 'name', 'city', 'role_id'],
            // offset: page,
            // limit: row,
        };

        const allStaffs = await Staff.findAll(options);

        res.status(200).json({
            status: 'success',
            result: allStaffs,
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        }) 
    }
}

const getStaffById = async (req, res) => {
    const foundStaff = await Staff.findByPk(req.params.id);

    if (!foundStaff) {
        return res.status(404).json(`Staff dengan id ${req.params.id} tidak ditemukan`)
    }
    res.status(200).json({
        status: 'Success',
        result: foundStaff
    })
}

const createStaff = async (req, res) => {
    try {
        const { name, city, role_id } = req.body;
    
        const createdStaff = await Staff.create({
            name: name,
            city: city,
            role_id: role_id
        });
        res.status(201).json({
            status: 'Success',
            result: createdStaff
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
    }
}

const updateStaff = async (req, res) => {
  try{
      const { name, city, role_id } = req.body;
    
      const updatedStaff = await Staff.update({
          name: name,
          city: city,
          role_id: role_id
      }, {
        where: {
          id: req.params.id
        } 
      });
      if (!updatedStaff[0]) {
        return res.status(404).json(`Staff dengan id ${req.params.id} tidak ditemukan`);
      }
      res.status(200).json({ 
          status: 'success',
          result: updatedStaff
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

const deleteStaff = async (req, res) => {
  try{
      const deletedStaff = await Staff.destroy({
          where: {
              id: req.params.id
          }
      });
      if (!deletedStaff) {
        res.status(404).json(`Staff dengan id ${req.params.id} tidak ditemukan`);
      }
      res.status(200).json({ 
          status: 'success',
          message: 'Yosha'
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

module.exports = {
  getAllStaffs,
  getStaffById,
  createStaff,
  updateStaff,
  deleteStaff
}