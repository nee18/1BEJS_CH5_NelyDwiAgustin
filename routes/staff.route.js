const express         = require('express');
const router          = express.Router();
const staffRoutes     = require('../controllers/staff.controller');
const staffValidation = require('../validation/staffs/staff.validation');
const validate        = require('../validation/validate');
const staffSanitizer  = require('../validation/staffs/staff.sanitizers');

router.get('/', staffRoutes.getAllStaffs);
router.get('/:id', staffRoutes.getStaffById);
router.post('/', staffValidation.create(), staffSanitizer.create(), validate, staffRoutes.createStaff);
router.put('/:id', staffValidation.create(), staffSanitizer.create(), validate, staffRoutes.updateStaff);
router.delete('/:id', staffRoutes.deleteStaff);

module.exports = router;