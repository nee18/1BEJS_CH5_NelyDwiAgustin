const express        = require('express');
const router         = express.Router();
const roleRoutes     = require('../controllers/role.controller');
const roleValidation = require('../validation/roles/role.validation');
const validate       = require('../validation/validate');
const roleSanitizer  = require('../validation/roles/role.sanitizers');

router.get('/', roleRoutes.getAllRoles);
router.get('/:id', roleRoutes.getRoleById);
router.post('/',  roleValidation.create(), roleSanitizer.create(), validate, roleRoutes.createRole);
router.put('/:id',  roleValidation.create(), roleSanitizer.create(), validate, roleRoutes.updateRole);
router.delete('/:id',  roleRoutes.deleteRole);

module.exports = router;