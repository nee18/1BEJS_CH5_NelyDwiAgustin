const express           = require('express');
const router            = express.Router();
const accountRoutes     = require('../controllers/account.controller');
const accountValidation = require('../validation/accounts/account.validation');
const validate          = require('../validation/validate');
const accountSanitizer  = require('../validation/accounts/account.sanitizers');

router.get('/', accountRoutes.getAllAccounts);
router.get('/:id',  accountRoutes.getAccountById);
router.post('/',  accountValidation.create(), accountSanitizer.create(), validate, accountRoutes.createAccount);
router.put('/:id',  accountValidation.create(), accountSanitizer.create(), validate, accountRoutes.updateAccount);
router.delete('/:id',  accountRoutes.deleteAccount);

module.exports = router;